using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight.FinderUIImplementations.ResultItems
{
    public class PivotTableResultItem : BaseResultItem
    {
        private readonly Worksheet parentWorksheet;
        private readonly PivotTable pivotTable;
        
        public override string Name => pivotTable.Name;
        public override string Location => $"in worksheet {parentWorksheet.Name}";
        public override BitmapImage Icon { get; }

        public PivotTableResultItem(
        	Worksheet parentWorksheet,
        	PivotTable pivotTable, 
        	System.Action closeWindowAction, 
        	BitmapImage icon,
        	IQueryStormLogger logger)
        	: base(logger, closeWindowAction)
	    {
            this.parentWorksheet = parentWorksheet;
            this.pivotTable = pivotTable;
            Icon = icon;
        }

        public override void ResultItemCommandAction()
        {
            try 
    		{	        
    			parentWorksheet.Activate();
        		pivotTable.TableRange1.Activate();
    		}
    		catch (Exception ex)
    		{
    			Logger.Error("Could not activate pivot table.");
    			Logger.Error(ex.Message);
    		}
    		finally
    		{
        		CloseWindowAction.Invoke();
    		}
        }
    }
}
