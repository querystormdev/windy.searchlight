using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight.FinderUIImplementations.ResultItems
{
    public class ControlResultItem : BaseResultItem
    {
        private readonly Worksheet parentWorksheet;
        private readonly Shape shape;

        public override string Name => shape.Name;
        public override string Location => $"in worksheet {parentWorksheet.Name}";
        public override BitmapImage Icon { get; }

        public ControlResultItem(
        	Worksheet parentWorksheet,
        	Shape shape, 
        	System.Action closeWindowAction, 
        	BitmapImage icon, 
        	IQueryStormLogger logger) 
        	: base(logger, closeWindowAction)
        {
            this.parentWorksheet = parentWorksheet;
            this.shape = shape;
            Icon = icon;
        }

        public override void ResultItemCommandAction()
        {
            try 
    		{
    			parentWorksheet.Activate();
    			shape.TopLeftCell.Activate();
    			shape.Select();
    		}
    		catch (Exception ex)
    		{
    			Logger.Error("Could not select control.");
    			Logger.Error(ex.Message);
    		}
    		finally
    		{
        		CloseWindowAction.Invoke();
    		}
        }
    }
}
