using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using FinderUI.ImplementationBase;
using FinderUI.UI;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using Windy.Searchlight.FinderUIImplementations;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight;

public class OpenSearchlightShortcut : ShortcutBindingBase
{
    private readonly Configuration cfg;
    private readonly IQueryStormLogger logger;
    private readonly IExcelAccessor excelAccessor;

    public override Key Key => cfg.Shortcut.Key;

    public override ModifierKeys ModifierKeys => cfg.Shortcut.ModifierKeys;

    public OpenSearchlightShortcut(Configuration cfg, IQueryStormLogger logger, IExcelAccessor excelAccessor)
    {
        this.cfg = cfg;
        this.logger = logger;
        this.excelAccessor = excelAccessor;
    }

	FinderWindow findWindow;
    public override void Execute()
    {
        if (findWindow == null)
        {
            AddIconsForUI();
            
            var excelItemsSource = new ExcelItemsSource(excelAccessor.Application, logger, () => findWindow.Close());
            var items = excelItemsSource.GetItems();
            findWindow = new FinderWindow((IntPtr)excelAccessor.Application.Hwnd, items);
            findWindow.Closed += (s, e) => findWindow = null;
            findWindow.ShowDialog();
        }
    }

    private void AddIconsForUI()
    {
        var currentAssembly = typeof(ExcelItemsSource).Assembly;
        ResultItemIcons.Add("sheet",
            currentAssembly.GetManifestResourceStream("ExcelSpotlight_sheet_20.png"));
        ResultItemIcons.Add("control",
            currentAssembly.GetManifestResourceStream("ExcelSpotlight_control_20.png"));
        ResultItemIcons.Add("range",
            currentAssembly.GetManifestResourceStream("ExcelSpotlight_range_20.png"));
        ResultItemIcons.Add("table",
            currentAssembly.GetManifestResourceStream("ExcelSpotlight_table_20.png"));
        ResultItemIcons.Add("pivot-table",
            currentAssembly.GetManifestResourceStream("ExcelSpotlight_pivottable_20.png"));
    }
}
