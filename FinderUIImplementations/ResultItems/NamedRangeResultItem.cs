using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight.FinderUIImplementations.ResultItems
{
    public class NamedRangeResultItem : BaseResultItem
    {
        private readonly Name namedRange;

        public override string Name => namedRange.Name;
        public override string Location => GetLocation();
        public override BitmapImage Icon { get; }
        
        public NamedRangeResultItem(
        	Name namedRange, 
        	System.Action closeWindowAction, 
        	BitmapImage icon,
        	IQueryStormLogger logger)
        	: base(logger, closeWindowAction)
        {
            this.namedRange = namedRange;
            Icon = icon;
        }

        public override void ResultItemCommandAction()
        {
            try 
    		{
    			var rng = namedRange.RefersToRange;
        		var ws = rng.Parent as Worksheet;
        		(ws as Worksheet).Activate();
        		rng.Activate();
    		}
    		catch (Exception ex)
    		{
    			Logger.Error("Named range is spanning multiple sheets or could not activate named range.");
    			Logger.Error(ex.Message);
    		}
    		finally
    		{
        		CloseWindowAction.Invoke();
    		}
        }
        
        private string GetLocation()
        {
        	try 
    		{
    			var rng = namedRange.RefersToRange;
        		var ws = rng.Parent as Worksheet;
        		return $"in worksheet {ws.Name}";
    		}
    		catch (Exception)
    		{
    			// hack:
    			// if a named range does not have a parent worksheet, 
    			// then it is a named range spanning multiple sheets.
    			// not sure if this works all of the time
    			return "in multiple worksheets";
    		}
        }
    }
}
