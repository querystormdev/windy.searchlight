using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight.FinderUIImplementations.ResultItems
{
	public class ListObjectResultItem : BaseResultItem
	{
        private readonly Worksheet parentWorksheet;
        private readonly ListObject lo;
		
        public override string Name => lo.DisplayName;
        public override string Location => $"in worksheet {parentWorksheet.Name}";
        public override BitmapImage Icon { get; }

        public ListObjectResultItem(
        	Worksheet parentWorksheet,
        	ListObject lo, 
        	System.Action closeWindowAction, 
        	BitmapImage icon, 
        	IQueryStormLogger logger)
        	: base(logger, closeWindowAction)
        {
            this.parentWorksheet = parentWorksheet;
            this.lo = lo;
            Icon = icon;
        }

        public override void ResultItemCommandAction()
        {
            try 
    		{	        
    			parentWorksheet.Activate();
        		lo.Range.Activate();
    		}
    		catch (Exception ex)
    		{
    			Logger.Error("Could not activate table.");
    			Logger.Error(ex.Message);
    		}
    		finally
    		{
        		CloseWindowAction.Invoke();
    		}
        }
    }
}
