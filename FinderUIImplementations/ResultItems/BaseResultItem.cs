using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using FinderUI.ImplementationBase;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight.FinderUIImplementations.ResultItems
{
    public abstract class BaseResultItem : IResultItem
    {
        public abstract string Name { get; }
        public abstract string Location { get; }
        public abstract BitmapImage Icon { get; }
        public ICommand ResultCommand => new ResultItemCommand(() => ResultItemCommandAction());
        public IQueryStormLogger Logger { get; }
        public Action CloseWindowAction { get; }

        public BaseResultItem(IQueryStormLogger logger, System.Action closeWindowAction)
        {
            Logger = logger;
            CloseWindowAction = closeWindowAction;
        }
        
        public abstract void ResultItemCommandAction();
    }
}
