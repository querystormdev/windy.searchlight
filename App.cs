using Microsoft.Office.Interop.Excel;
using System;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using Unity;
using Unity.Lifetime;
using System.Windows.Input;
using FinderUI.UI;
using Windy.Searchlight.FinderUIImplementations;
using FinderUI.ImplementationBase;
using Thingie.WPF.Controls.PropertiesEditor;
using Thingie.WPF;

namespace Windy.Searchlight
{
    public class App : AppBase
    {
        public App(IAppHost appHost)
            : base(appHost)
        {
        }
    }
}