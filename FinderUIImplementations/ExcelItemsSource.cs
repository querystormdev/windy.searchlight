using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windy.Searchlight.FinderUIImplementations.ResultItems;
using FinderUI.ImplementationBase;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight.FinderUIImplementations
{
	public class ExcelItemsSource : IItemsSource
	{
		private readonly Application excelApp;
        private readonly IQueryStormLogger logger;
        private readonly System.Action closeWindowAction;

        public ExcelItemsSource(Application app, IQueryStormLogger logger, System.Action closeWindowAction)
        {
            this.excelApp = app;
            this.logger = logger;
            this.closeWindowAction = closeWindowAction;
        } 

        public IEnumerable<IResultItem> GetItems()
        {
        	var items = new List<IResultItem>();
            items.AddRange(CreateWorksheetResultItems());
            items.AddRange(CreateListObjectResultItems());
            items.AddRange(CreatePivotTableResultItems());
            items.AddRange(CreateNamedRangeResultItems());
            items.AddRange(CreateControlResultItems());
            return items;
        }
        
        private IEnumerable<IResultItem> CreateWorksheetResultItems()
        {
        	var worksheetItems = new List<IResultItem>();
        	
        	var workbook = excelApp.ActiveWorkbook;
            var sheets = workbook.Worksheets as Sheets;
            var icon = ResultItemIcons.FinderIcons["sheet"];
            
            foreach (Worksheet sheet in sheets)
            {
        	 	worksheetItems.Add(new WorksheetResultItem(sheet, closeWindowAction, icon, logger));
            }
            
            return worksheetItems;
        }
        
        private IEnumerable<IResultItem> CreateListObjectResultItems()
        {
        	var listObjectItems = new List<IResultItem>();
        	
        	var workbook = excelApp.ActiveWorkbook;
            var sheets = workbook.Worksheets as Sheets;
            var icon = ResultItemIcons.FinderIcons["table"];
            
            foreach (Worksheet sheet in sheets)
            {
            	 foreach (ListObject lo in sheet.ListObjects)
            	 {
            	 	 listObjectItems.Add(new ListObjectResultItem(sheet, lo, closeWindowAction, icon, logger));
            	 }
            }
            
            return listObjectItems;
        }
        
        private IEnumerable<IResultItem> CreatePivotTableResultItems()
        {
        	var pivotTableItems = new List<IResultItem>();
        	
        	var workbook = excelApp.ActiveWorkbook;
            var sheets = workbook.Worksheets as Sheets;
            var icon = ResultItemIcons.FinderIcons["pivot-table"];
            
            foreach (Worksheet sheet in sheets)
            {
            	 foreach (PivotTable pivotTable in (PivotTables)sheet.PivotTables(Type.Missing))
            	 {
            	 	 pivotTableItems.Add(new PivotTableResultItem(sheet, pivotTable, closeWindowAction, icon, logger));
            	 }
            }
            
            return pivotTableItems;
        }
        
        private IEnumerable<IResultItem> CreateNamedRangeResultItems()
        {
        	var namedRangeItems = new List<IResultItem>();
        	
        	var workbook = excelApp.ActiveWorkbook;
            var icon = ResultItemIcons.FinderIcons["range"];
            
            foreach (Name name in workbook.Names)
            {
        	 	 namedRangeItems.Add(new NamedRangeResultItem(name, closeWindowAction, icon, logger));
            }
            
            return namedRangeItems;
        }
        
        private IEnumerable<IResultItem> CreateControlResultItems()
        {
        	var controlItems = new List<IResultItem>();
        	
        	var workbook = excelApp.ActiveWorkbook;
            var sheets = workbook.Worksheets as Sheets;
            var icon = ResultItemIcons.FinderIcons["control"];
            
            foreach (Worksheet sheet in sheets)
            {
            	 foreach (Shape shape in sheet.Shapes)
            	 {
            	 	 controlItems.Add(new ControlResultItem(sheet, shape, closeWindowAction, icon, logger));
            	 }
            }
            
            return controlItems;
        }
	}
}
