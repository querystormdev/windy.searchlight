using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Jot.Configuration.Attributes;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using Thingie.WPF;
using Thingie.WPF.Controls.PropertiesEditor.DefaultFactory.Attributes;
using Thingie.WPF.KeyboardShortcuts;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight
{
    public class Configuration : ISettings
    {
        [EditableShortcut, Trackable]
        public ShortcutGesture Shortcut { get; set; } = new ShortcutGesture(Key.OemComma, ModifierKeys.Control);

        public void OnUpdated()
        {
            throw new NotImplementedException();
        }
    }
}
