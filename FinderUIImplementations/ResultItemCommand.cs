using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight.FinderUIImplementations
{
    public class ResultItemCommand : ICommand
    {
        private readonly Action commandAction;

        public ResultItemCommand(Action commandAction)
    	{
            this.commandAction = commandAction;
        }
        
        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            commandAction.Invoke();
        }
    }
}
