using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Searchlight.FinderUIImplementations.ResultItems
{
	public class WorksheetResultItem : BaseResultItem
	{
		private readonly Worksheet worksheet;
		
		public override string Name => worksheet.Name;
        public override string Location => "in current workbook";
        public override BitmapImage Icon { get; }

        public WorksheetResultItem(
        	Worksheet worksheet, 
        	System.Action closeWindowAction, 
        	BitmapImage icon,
        	IQueryStormLogger logger)
        	: base(logger, closeWindowAction)
        {
            this.worksheet = worksheet;
            Icon = icon;
        }

        public override void ResultItemCommandAction()
        {
            try 
    		{	        
    			worksheet.Activate();
    		}
    		catch (Exception ex)
    		{
    			Logger.Error("Could not activate sheet.");
    			Logger.Error(ex.Message);
    		}
    		finally
    		{
        		CloseWindowAction.Invoke();
    		}
        }
    }
}
